import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RegistroFormComponent } from './registro-form/registro-form.component';



@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [RegistroFormComponent],
  exports: [RegistroFormComponent]
})
export class RegistroModule { }
