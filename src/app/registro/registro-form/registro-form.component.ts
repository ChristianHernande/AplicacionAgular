import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-registro-form',
  templateUrl: './registro-form.component.html',
  styleUrls: ['./registro-form.component.scss']
})
export class RegistroFormComponent implements OnInit {

  public usuarioRegistro: Usuario = new Usuario(null,null);
  public listaUsuarios: Array<Usuario> = [];

  constructor() { 
    console.log("constructor ejecutado");
  }

  ngOnInit() {
    console.log("componente ejecutado")
  }

  registroUsuario() {
    this.listaUsuarios.push({
      nombre: this.usuarioRegistro.nombre,
      apellido: this.usuarioRegistro.apellido
    });
  }

}

class Usuario {
  nombre: string;
  apellido: string;

  constructor(nombre:string, apellido:string){
      this.nombre = nombre;
      this.apellido = apellido;
  }
}

